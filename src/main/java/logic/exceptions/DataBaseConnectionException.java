package logic.exceptions;

public class DataBaseConnectionException extends Throwable {
    public DataBaseConnectionException(String message){
        super(message);
    }
}
