package logic.exceptions;

public class DuplicateInformationException extends Throwable {
    public DuplicateInformationException(String message) {
        super(message);
    }
}
