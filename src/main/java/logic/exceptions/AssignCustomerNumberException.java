package logic.exceptions;

public class AssignCustomerNumberException extends Throwable {
    public AssignCustomerNumberException(String message) {
        super(message);
    }
}
