package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtil {

    private static Connection connection = null;

    private ConnectionUtil() {}

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank_database?useUnicode=true&characterEncoding=UTF-8&useSSL=false", "root", "");
                Statement statement =  connection.createStatement();
                statement.execute("CREATE TABLE SAMPLE (customerNumber int)");

            System.out.println("successfully");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("خطا در اتصال");
            e.printStackTrace();
        }
    }
    public static Connection getConnectionUtil() {
        return connection;
    }


    public static void main(String[] args) {
        new ConnectionUtil();
    }




}
