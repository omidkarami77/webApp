-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2021 at 08:55 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `customernumber` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customernumber`) VALUES
(3, '1615644211180'),
(4, '1615644276697'),
(5, '1615644448482'),
(6, '1615645680865'),
(7, '1615645736665'),
(9, '1615646234747'),
(10, '1615646971704'),
(12, '1616053394502'),
(13, '1616053548950'),
(14, '1616053575973');

-- --------------------------------------------------------

--
-- Table structure for table `legalcustomer`
--

CREATE TABLE `legalcustomer` (
  `id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `dateOfRegistration` varchar(20) NOT NULL,
  `economicCode` varchar(20) NOT NULL,
  `legalcustomernumber` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `legalcustomer`
--

INSERT INTO `legalcustomer` (`id`, `name`, `dateOfRegistration`, `economicCode`, `legalcustomernumber`) VALUES
(4, 'wwe', '1400', '1239456789', '1615644276697'),
(7, 'sajjad hormoz', '1401', '793024589348', '1615645736665'),
(12, 'uaaa', '1320', '89789r7e89t7', '1616053394502'),
(14, 'tna', '1346', '847598734435', '1616053575973');

-- --------------------------------------------------------

--
-- Table structure for table `realcustomer`
--

CREATE TABLE `realcustomer` (
  `id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `family` varchar(30) NOT NULL,
  `fathername` varchar(30) NOT NULL,
  `dateofbirth` varchar(20) NOT NULL,
  `nationalcode` varchar(20) NOT NULL,
  `realcustomernumber` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `realcustomer`
--

INSERT INTO `realcustomer` (`id`, `name`, `family`, `fathername`, `dateofbirth`, `nationalcode`, `realcustomernumber`) VALUES
(3, 'navid', 'karami', 'hamid', '1384', '0100959532', '1615644211180'),
(5, 'saeeid', 'karami', 'vahid', '1333', '0987654321', '1615644448482'),
(6, 'reza', 'hormoznejad', 'ali', '1377', '84957893479', '1615645680865'),
(9, 'morteza', 'jafari', 'alireza', '11223', '45345456456', '1615646234747'),
(10, 'ali', 'alavi', 'nader', '1234', '3459834934895', '1615646971704'),
(13, 'ali', 'miri', 'hasan', '1345', '5560474509', '1616053548950');

-- --------------------------------------------------------

--
-- Table structure for table `sample`
--

CREATE TABLE `sample` (
  `customerNumber` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realcustomer`
--
ALTER TABLE `realcustomer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
